import pcbnew

class DRCError:
    def __init__(self, severity = 0, text =  "", value_is = None, value_should = None, items = []):
        self.severity = severity
        self.text = text
        self.value_is = value_is
        self.value_should = value_should
        self.items = items

    def __str__(self, conv = pcbnew.ToMM):
        text = f"[{self.severity}] '{self.text}'"
        if self.value_is is not None:
            text = text + f": {round(conv(self.value_is), 3)} < {round(conv(self.value_should), 3)}"
        return text

    def __bool__(self):
        return -1 < self.severity < 1

class DRCChecker():
    ListOfRules = dict()
    ListOfResults = list()
    ListOfParameters = dict()
    debug_output = None

    def Rule(display_name = "", description = "", parameters = None, icon = None, match = lambda items: False):
        def DecoratorRule(rule):
            def rule_wrapper(items, *args, **kwargs):
                results = rule(items, *args, **kwargs)
                for r in results:
                    r.items = items + r.items
                    DRCChecker.ListOfResults.append({'rule': rule.__name__, 'result': r})
            DRCChecker.ListOfRules[rule.__name__] = {'rule': rule_wrapper, 'display_name' : display_name, 'icon' : icon, 'match':match, 'description':description}
            DRCChecker.ListOfParameters[rule.__name__] = parameters
            return rule_wrapper
        return DecoratorRule

    def RunExtraRules(items, *args, **kwargs):
        for name in DRCChecker.ListOfRules:
            rule = DRCChecker.ListOfRules[name]['rule']
            if DRCChecker.ListOfRules[name]['match'](items):
                if DRCChecker.ListOfParameters[name] is not None:
                    rule(items, DRCChecker.ListOfParameters[name], *args, **kwargs)
                else:
                    rule(items, *args, **kwargs)

    def ListExtraRules():
        for name in DRCChecker.ListOfRules:
            print(f"{name}")

    def Run(board, output, selected_only = False):
        DRCChecker.debug_output = output

        if selected_only:
            DRCChecker.debug("Running DRC Only on selected items.\n")

        any_selected = False
        DRCChecker.ListOfResults = list()
        for item in board.GetFootprints():
            if selected_only and not item.IsSelected():
                continue
            any_selected = True
            DRCChecker.debug(f"Checking Footprint '{item.GetReference()}'...\n")
            DRCChecker.RunExtraRules([item], board)

        for item in board.GetPads():
            if selected_only and not item.IsSelected():
                continue
            any_selected = True
            DRCChecker.debug(f"Checking Pad '{item.GetPadName()}'...\n")
            DRCChecker.RunExtraRules([item], board)
        if not any_selected:
            DRCChecker.debug("No items were selected.\n")

    def debug(text):
        if DRCChecker.debug_output is not None:
            DRCChecker.debug_output.AppendText(text)

    def PrintResults():
        for result in DRCChecker.ListOfResults:
            error = result['result']
            print(f"rule '{result['rule']}': {error}")

    def MatchFootprints(items):
        return len(items) == 1 and items[0].GetTypeDesc() == 'Footprint'

    def MatchPads(items):
        return len(items) == 1 and items[0].GetTypeDesc() == 'Pad'

    def MatchZone(items):
        return len(items) == 1 and items[0].GetTypeDesc() == 'Zone'
