if __name__ == "__main__":
    class MyApp(wx.App):
        def OnInit(self):
            extra_drc = RunExtraDRCPlugin()
            extra_drc.Run()
            return True

    app = MyApp(0)
    app.MainLoop()
else:
    from .RunExtraDRCPlugin import RunExtraDRCPlugin
    RunExtraDRCPlugin().register()