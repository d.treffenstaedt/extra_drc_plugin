from .drc_checker import DRCError,DRCChecker
import pcbnew
import os

def get_tracks_at(position, layer, tracks):
    tracks_out = []
    for t in tracks:
        if t.Type() == 19 and t.GetPosition() == position:
            tracks_out.append([t, 'via'])
        elif t.GetStart() == position and (layer == None or t.GetLayer() == layer):
            tracks_out.append([t, 'start'])
        elif t.GetEnd() == position and (layer == None or t.GetLayer() == layer):
            tracks_out.append([t, 'end'])
    return tracks_out

def track_width_connected_pad_impl(items, parameters, board):
    """DRC Rule to check
    """

    layer = None
    if not items[0].HasHole():
        layer = items[0].GetLayer()

        if items[0].IsFlipped():
            layer = 31 - layer

    tracks = get_tracks_at(items[0].GetPosition(), layer, board.GetTracks())

    if len(tracks) == 0:
        return []

    for t in tracks:
        if t[1] == 'via':
            continue
        if t[0].GetWidth() / items[0].GetSizeX() > parameters['Max ratio']:
            return [DRCError(0, f"Track connected to pad too wide.", t[0].GetWidth(),items[0].GetSizeX() * parameters['Max ratio'], [t[0]])]
        if t[0].GetWidth() / items[0].GetSizeY() > parameters['Max ratio']:
            return [DRCError(0, f"Track connected to pad too wide.", t[0].GetWidth(), items[0].GetSizeY() * parameters['Max ratio'], [t[0]])]
    return []

@DRCChecker.Rule(
    display_name="Copper Zone Hatching",
    description="ECSS-Q-ST-70-12C Section x.xx\nCopper areas above a certain size should be hatched.",
#    match=lambda items: DRCChecker.MatchZone(items),
    parameters={'Area Cutoff' : 10, 'Hatching gap' : 0.381, 'Hatching width' : 0.381},
    icon=os.path.join(os.path.dirname(__file__), 'icons/copper_zone_hatching.png')
)
def copper_zone_hatching(items, parameters, board):
    """DRC Rule to check
    """
    return []


@DRCChecker.Rule(
    display_name="Track width PTH",
    description="ECSS-Q-ST-70-12C Section x.xx\nMaximum Track size connected to PTH.",
    match=lambda items: DRCChecker.MatchPads(items) and items[0].HasHole(),
    parameters={'Max ratio' : 0.5},
    icon=os.path.join(os.path.dirname(__file__), 'icons/track_width_connected_pad.png')
)
def track_width_connected_pad_pth(items, parameters, board):
    return track_width_connected_pad_impl(items, parameters, board)

@DRCChecker.Rule(
    display_name="Track width chip component",
    description="ECSS-Q-ST-70-12C Section x.xx\nMaximum Track size connected to Chip component pad (taken as two-pad smd component).",
    match=lambda items: DRCChecker.MatchPads(items) and not items[0].HasHole() and len(items[0].GetParent().Pads()) == 2,
    parameters={'Max ratio' : 0.35},
    icon=os.path.join(os.path.dirname(__file__), 'icons/track_width_connected_pad_chip.png')
)
def track_width_connected_pad_chip(items, parameters, board):
    return track_width_connected_pad_impl(items, parameters, board)

@DRCChecker.Rule(
    display_name="Track width flatpak component",
    description="ECSS-Q-ST-70-12C Section x.xx\nMaximum Track size connected to smd component pad (taken as many-pad smd component).",
    match=lambda items: DRCChecker.MatchPads(items) and not items[0].HasHole() and len(items[0].GetParent().Pads()) > 2,
    parameters={'Max ratio' : 0.9},
    icon=os.path.join(os.path.dirname(__file__), 'icons/track_width_connected_pad_smd.png')
)
def track_width_connected_pad_smd(items, parameters, board):
    return track_width_connected_pad_impl(items, parameters, board)

@DRCChecker.Rule(
    display_name="Connection width skew",
    description="ECSS-Q-ST-70-12C Section x.xx\nDifference in track thickness connected to chip component.",
    match=lambda items: DRCChecker.MatchFootprints(items) and len(items[0].Pads()) == 2,
    parameters={'Max skew' : 0.1},
    icon=os.path.join(os.path.dirname(__file__), 'icons/skew.png')
    )
def footprint_connection_width_skew(items, parameters, board):
    pads = items[0].Pads()

    if len(pads) != 2:
        return []

    width = 0
    for p in pads:
        tracks = get_tracks_at(p.GetPosition(), p.GetLayer(), board.GetTracks())

        w = 0

        if len(tracks) == 0:
            w = p.GetThermalSpokeWidth()
            tracks = [None]

        for t in tracks:
            if t != None:
                w = t[0].GetWidth()
            if width == 0:
                width = w
            elif 2*abs(w - width)/(w + width) > parameters['Max skew']:
                return [DRCError(0, f"Track skew too high for footprint '{items[0].GetReference()}'.", 2*abs(w - width)/(w + width), parameters['Max skew'])]

    return []

def pad_via_distance(pad, layer, min_length, tracks, initial_data):

    unvisited_tracks = tracks

    track_ends = initial_data

    while len(unvisited_tracks) > 0:
        new_ends = []
        shortest = -1
        for end in track_ends:
            tracks = get_tracks_at(end['pos'], layer, unvisited_tracks)
            if len(tracks) == 0:
                continue
            preliminary_ends = []
            n = 0
            for t in tracks:
                unvisited_tracks.remove(t[0])

                if t[1] == 'via':
                    length = end['len'] - t[0].GetWidth()/2
                    if length < min_length:
                        return [DRCError(0, "Distance between Pad and Via too short", length, min_length, [t[0]])]
                    preliminary_ends.clear()
                    break

                length = end['len'] + (t[0].GetEnd() - t[0].GetStart()).EuclideanNorm()

                if length > min_length * 2:
                    continue

                if length < shortest or shortest == -1:
                    shortest = length

                if t[1] == 'end':
                    preliminary_ends.append({'pos':t[0].GetStart(), 'len': length, 'n': n})
                elif t[1] == 'start':
                    preliminary_ends.append({'pos':t[0].GetEnd(), 'len': length, 'n': n})
                n = n +1
            new_ends = new_ends + preliminary_ends
        if len(new_ends) == 0:
            return []
        track_ends = new_ends
    return []

@DRCChecker.Rule(
    display_name="Fan out length",
    description="ECSS-Q-ST-70-12C Section x.xx\nFan out from pad to via. Minimum distance depends on type and width of pad.",
    match=lambda items: DRCChecker.MatchPads(items),
    icon=os.path.join(os.path.dirname(__file__), 'icons/pad_via_distance.png')
)
def pad_via_fan_out_distance(items, board):
    """ Minimum open track length between a pad and a via.
        This is essentially a slightly modified Dijkstra algorithm.
    """

    track_ends = [{'pos' : items[0].GetPosition(), 'len' : -(items[0].GetSizeX() if (items[0].GetSizeX()>items[0].GetSizeY()) else items[0].GetSizeY())/2, 'n':0}]
    min_length = pcbnew.FromMM(0.7)

    layer = items[0].GetLayer()

    if items[0].IsFlipped():
        layer = 31 - layer

    tracks = board.GetTracks()

    if items[0].HasHole():
        min_length = items[0].GetSizeX() / 2
        layer = None
    else:
        connected_tracks = get_tracks_at(items[0].GetPosition(), layer, tracks)
        if len(connected_tracks) == 0:
            return []
        if connected_tracks[0][0].GetWidth() >= 0.5:
            min_length = pcbnew.FromMM(0.7)
        else:
            min_length = pcbnew.FromMM(0.5)

    errors = pad_via_distance(items[0], layer, min_length, tracks, track_ends)
    if len(errors) > 0:
        for i, error in enumerate(errors):
            error.text = error.text + f" in Pad {items[0].GetPadName()} of footprint {items[0].GetParent().GetReference()}"
            errors[i] = error
    return errors


