import wx
from .RunExtraDRCDialog import RunExtraDRCDialog

from .drc_rules import *
from .drc_checker import DRCChecker
import pcbnew
import os


class RunExtraDRCPlugin(pcbnew.ActionPlugin):
    def __init__(self):
        self.running = False
        self.name = "Extra DRC Rules"
        self.category = "DRC Rules"
        self.description = "Run extra custom DRC rules"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'logo.png')
        self.dark_icon_file_name = os.path.join(os.path.dirname(__file__), 'logo.png')
        self.text = ""

    def defaults(self):
        self.running = False
        self.name = "Extra DRC Rules"
        self.category = "DRC Rules"
        self.description = "Run extra custom DRC rules"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'logo.png')
        self.dark_icon_file_name = os.path.join(os.path.dirname(__file__), 'logo.png')
        self.text = ""

    def Run(self):
        self.running = False
        self.RunExtraDRCDialog = RunExtraDRCDialog(None, wx.ID_ANY, "")

        self.labels = [None]*4
        self.spin_controls = [None]*4

        for i in range(4):
            self.labels[i] = wx.StaticText(self.RunExtraDRCDialog.panel_1, wx.ID_ANY | wx.RESERVE_SPACE_EVEN_IF_HIDDEN, "")

            self.spin_controls[i] = wx.SpinCtrlDouble(self.RunExtraDRCDialog.panel_1, wx.ID_ANY | wx.RESERVE_SPACE_EVEN_IF_HIDDEN, initial=0, min=0.0, max=100.0)
            self.spin_controls[i].SetDigits(2)

            self.RunExtraDRCDialog.drc_settings.Add(self.labels[i], (i, 0), (1, 1), 0, 0)
            self.RunExtraDRCDialog.drc_settings.Add(self.spin_controls[i], (i, 1), (1, 1), 0, 0)

            self.labels[i].Hide()
            self.spin_controls[i].Hide()

        for rule_name in DRCChecker.ListOfRules:
            self.RunExtraDRCDialog.drc_rules.Append(DRCChecker.ListOfRules[rule_name]['display_name'])

        self.RunExtraDRCDialog.Bind(wx.EVT_LISTBOX, self.select_rule, self.RunExtraDRCDialog.drc_rules)
        self.RunExtraDRCDialog.Bind(wx.EVT_BUTTON, self.run_rules, self.RunExtraDRCDialog.button_Run)
        self.RunExtraDRCDialog.drc_icon_pixmap.SetBitmap(wx.Bitmap(os.path.join(os.path.dirname(__file__), 'icons/empty.png'), wx.BITMAP_TYPE_ANY))

        self.RunExtraDRCDialog.drc_rules.Select(0)

        self.select_rule(None)

        if self.text is None:
            self.text = ""
        self.RunExtraDRCDialog.drc_output.AppendText(self.text)
        self.RunExtraDRCDialog.ShowModal()
        self.RunExtraDRCDialog.Destroy()
        return True

    def update_parameter(self, rule_name, param_name, value):
        DRCChecker.ListOfParameters[rule_name][param_name] = value

    def select_rule(self, event):
        sel = self.RunExtraDRCDialog.drc_rules.GetSelection()
        text = self.RunExtraDRCDialog.drc_rules.GetString(sel)
        rule_id = ""
        for rule in DRCChecker.ListOfRules:
            if DRCChecker.ListOfRules[rule]['display_name'] == text:
                rule_id = rule
                break
        parameters = DRCChecker.ListOfParameters[rule_id]
        icon = DRCChecker.ListOfRules[rule_id]['icon']
        description = DRCChecker.ListOfRules[rule_id]['description']

        for label in self.labels:
            label.Enable(False)
            label.SetLabel("")
            label.Hide()
        for ctrl in self.spin_controls:
            ctrl.Enable(False)
            ctrl.SetValue(0.0)
            ctrl.Hide()

        if icon is not None:
            self.RunExtraDRCDialog.drc_icon_pixmap.SetBitmap(wx.Bitmap(icon, wx.BITMAP_TYPE_ANY))
            self.RunExtraDRCDialog.drc_icon_pixmap.Show()
            self.RunExtraDRCDialog.drc_icon_pixmap.Update()
        else:
            self.RunExtraDRCDialog.drc_icon_pixmap.SetBitmap(wx.Bitmap(os.path.join(os.path.dirname(__file__), 'icons/empty.png'), wx.BITMAP_TYPE_ANY))

        self.RunExtraDRCDialog.drc_description.SetValue(description)


        if parameters is not None:
            for i, param_name in enumerate(parameters):
                self.labels[i].SetLabel(param_name)
                self.spin_controls[i].SetValue(parameters[param_name])
                self.spin_controls[i].Enable(True)
                self.labels[i].Enable(True)
                self.RunExtraDRCDialog.Bind(wx.EVT_SPINCTRLDOUBLE, lambda e: self.update_parameter(text, param_name, self.spin_controls[i].GetValue()), self.spin_controls[i])

                self.labels[i].Show()
                self.spin_controls[i].Show()

    def run_rules(self, event):
        self.text = self.RunExtraDRCDialog.drc_output.SetValue("")
        if self.running:
            self.RunExtraDRCDialog.drc_output.AppendText("Already running.\n")
            return
        self.running = True
        self.RunExtraDRCDialog.drc_output.AppendText(os.getcwd())

        self.RunExtraDRCDialog.drc_output.AppendText("running rules...\n")
        board = pcbnew.GetBoard()

        self.RunExtraDRCDialog.drc_output.AppendText("loaded board.\n")

        DRCChecker.Run(board, self.RunExtraDRCDialog.drc_output, self.RunExtraDRCDialog.drc_only_selected_items.IsChecked())
        self.RunExtraDRCDialog.drc_output.AppendText(f"Results: [{len(DRCChecker.ListOfResults)}]\n")

        for result in DRCChecker.ListOfResults:
            self.RunExtraDRCDialog.drc_output.AppendText(f"\nRule {result['rule']}: {result['result']}")
            for item in result['result'].items:
                item.SetBrightened()
        try:
            self.text = self.RunExtraDRCDialog.drc_output.GetValue()
        except e:
            print("error")
        if self.text is None:
            self.text = ""
        self.running = False

